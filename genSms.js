function prepQty(s)
{
	var num = parseInt(s);
	switch(num)
	{
		case 1: return "1";
		case 2: return "TWO";
		case 3: return "THREE";
		case 4: return "FOUR";
		case 5: return "FIVE";
		default: return "We only support up to 5. Please edit genSms.js to add more numbers";
	}
}

function getCounter(reuseCounter)
{
	var i = localStorage["counter"];
	if (i)
	{
		if (!reuseCounter)
			i++;
	}
	else
		i = 0;
	
	localStorage["counter"] = i;
	document.getElementById("counter").value = localStorage["counter"];
	return i;
}

function generateSms(reuseCounter)
{
	var d = new Date(); // TODO: calc this
	var date = d.getMonth()+1 + "/" + d.getDate();
	var count = getCounter(reuseCounter);
	var consignee = document.getElementsByName("consignee_name")[0].value
	var address = document.getElementsByName("consignee_address")[0].value
	var phone = document.getElementsByName("consignee_telno")[0].value
	var waybillNum = document.getElementsByName("waybill_no")[0].value;
	
	var packages = "";
	$("input#packagedesc").each(function (index)
	{
		var qty = prepQty($("input#packageqty")[index].value);
		var desc = $("input#packagedesc")[index].value;
		
		if (!desc)
			return false;
		
		if (packages)
			packages += ",";
		packages += qty + " " + desc;
	});
	
	var sms = "PU " + date + " #" + count + " " + waybillNum + " " + consignee + "/" + address + "/" + phone + "/" + packages + ".";
	document.getElementById("smsText").value = sms;

}

function saveSms()
{
	var text = (new Date()).toString() + "\n" + document.getElementById("smsText").value;
	chrome.extension.sendMessage({saveToHistory: text});
	document.getElementById("smsText").select();
	document.execCommand("Copy");
}

function saveCounter()
{
	var counter = parseInt(document.getElementById("counter").value);
	if (counter > 0)
		localStorage["counter"] = counter;
	else
		alert("Invalid counter value, not updating!");
		
	document.getElementById("counter").value = localStorage["counter"];
}

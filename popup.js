function sendWaybill()
{
	console.log("creating new tab for shipper");
	chrome.tabs.create({
		'url':'http://www.jamglobalexpress.com/login.php',
		'selected':true
	});	
		
	chrome.tabs.executeScript(null, {file: "login.js"}, function () {
		chrome.tabs.update({
			'url':'http://www.jamglobalexpress.com/CreateNewWaybill.php',
			'selected':true
		});	

		console.log("filling waybill");
		chrome.tabs.executeScript(null, {file: "fillWaybill.js"});
	});
	console.log("logging in to shipper");
}
/*
function fillWaybill()
{
	chrome.tabs.update({
		'url':'http://www.jamglobalexpress.com/CreateNewWaybill.php',
		'selected':true
	});	

	console.log("filling waybill");
	chrome.tabs.executeScript(null, {file: "fillWaybill.js"});
}
*/
function showHistory()
{
	console.log("displaying history");
	chrome.tabs.create({
		'selected':true
		}, function() {
			document.write("Hello");
		}
	);	
	chrome.tabs.executeScript(null, {file: "showHistory.js"});		
}

document.addEventListener('DOMContentLoaded', function () {
  document.querySelector('button[id=btnCreateWayBill]').addEventListener('click', sendWaybill);
  document.querySelector('button[id=btnShowHistory]').addEventListener('click', showHistory);
});

chrome.extension.onMessage.addListener(
  function(request, sender, sendResponse) {
	console.log("received message: " + request.saveToHistory);
	var oldHistory = chrome.storage.sync.get("history");
	chrome.storage.sync.set({'history': oldHistory + "-----" + request.saveToHistory });
  });


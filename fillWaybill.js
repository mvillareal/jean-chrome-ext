function create(htmlStr) {
    var frag = document.createDocumentFragment(),
        temp = document.createElement('div');
    temp.innerHTML = htmlStr;
    while (temp.firstChild) {
        frag.appendChild(temp.firstChild);
    }
    return frag;
}

//alert(window.location.href);
/*
document.addEventListener('DOMContentLoaded', function () {
  document.querySelector('button[id=btnGenerateSMS]').addEventListener('click', generateSms);
});
*/
var s = document.createElement('script');
s.src = chrome.extension.getURL('genSms.js');
(document.head||document.documentElement).appendChild(s);
s.onload = function() {
    s.parentNode.removeChild(s);
};

var fragment = create('<textArea rows="2" cols="50" id="smsText"></textarea><button type="button" id="btnGenerateSMS" onClick="javascript:generateSms(false)">Generate SMS</button><button type="button" id="btnRegenerateSMS" onClick="javascript:generateSms(true)">Regenerate SMS (Reuse counter)</button><button type="button" id="btnSave" onClick="javascript:saveSms()">Save and Copy to Clipboard</button><br/>Current counter: <input type="text" id="counter" size="2"/><button type="button" id="btnSaveCounter"onClick="saveCounter()">Update Counter</button>');

document.body.insertBefore(fragment, document.body.childNodes[20]);

document.getElementsByName("shipper_origin")[0].value = "Mandaluyong City";

if (document.getElementById("counter"))
{
	var counter = localStorage["counter"];
	if (!counter)
		counter = 1;
	document.getElementById("counter").value = counter;
}
/*
chrome.extension.sendMessage({waybillNum: waybillNum}, function(response) {
  console.log("send waybill to extension: " + waybillNum);
});*/